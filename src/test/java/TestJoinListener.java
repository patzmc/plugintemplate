import static org.junit.jupiter.api.Assertions.assertEquals;

import com.patzmc.plugin.Main;

import org.junit.jupiter.api.Test;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;

public class TestJoinListener {
  @Test
  public void testPlayerPos() {
    ServerMock server = MockBukkit.mock();
    MockBukkit.load(Main.class);
    PlayerMock player = server.addPlayer();
    assertEquals(50, player.getLocation().getY());
  }
}
